package com.gemframework.modules.extend.student.service.impl;


import com.gemframework.GemAdminApplication;
import com.gemframework.modules.extend.student.entity.Student;
import com.gemframework.modules.extend.student.mapper.StudentMapper;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.stereotype.Service;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = GemAdminApplication.class)
@Slf4j
@Service
class StudentServiceImplTest {

    @Resource
    private StudentMapper studentMapper;

    @Test
    void insert1() {

        Student student = new Student();
        student.setName("测试1");
        studentMapper.insert(student);
        log.info("保存成功！");
    }

    @Test
    void insert2() {
        Student student = new Student();
        student.setName("测试2");
        studentMapper.insert(student);
        log.info("保存成功！");
    }

    @Test
    void insert3() {
        studentMapper.insert1("测试1");
        log.info("保存成功！");
    }

    @Test
    void insert4() {
        studentMapper.insert2("c===测试2");
        log.info("保存成功！");
    }
}