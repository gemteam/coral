/**
 * 严肃声明：
 * 开源版本请务必保留此注释头信息，若删除捷码开源〔GEMOS〕官方保留所有法律责任追究！
 * 本软件受国家版权局知识产权以及国家计算机软件著作权保护（登记号：2018SR503328）
 * 不得恶意分享产品源代码、二次转售等，违者必究。
 * Copyright (c) 2020 gemframework all rights reserved.
 * http://www.gemframework.com
 * 版权所有，侵权必究！
 */
package com.gemframework.common.config.mybatis;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import com.gemframework.common.config.shiro.ShiroUtils;
import com.gemframework.model.entity.po.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import java.util.Date;


@Component
@Slf4j
public class MetaDataHandler implements MetaObjectHandler {

    @Override
    public void insertFill(MetaObject metaObject) {
        log.info("admin start insert field....");
        this.setFieldValByName("createTime", new Date(), metaObject);
        this.setFieldValByName("updateTime", new Date(), metaObject);
        log.info("metaObject="+metaObject.toString());
        // User user = ShiroUtils.getUser();
        // Long userId = 0L;
        // if(user!=null){
        //     userId = user.getId();
        // }
        //this.setFieldValByName("createId", userId, metaObject);
        //this.setFieldValByName("updateId", userId, metaObject);
    }



    @Override
    public void updateFill(MetaObject metaObject) {
        log.info("admin start update field....");
        // User user = ShiroUtils.getUser();
        // Long userId = 0L;
        // if(user!=null){
        //     userId = user.getId();
        // }
        this.setFieldValByName("updateTime", new Date(),metaObject);
        //this.setFieldValByName("updateId", userId,metaObject);
    }
}