/**
 * 开源版本请务必保留此注释头信息，若删除捷码开源〔GEMOS〕官方保留所有法律责任追究！
 * 本软件受国家版权局知识产权以及国家计算机软件著作权保护（登记号：2018SR503328）
 * 不得恶意分享产品源代码、二次转售等，违者必究。
 * Copyright (c) 2020 gemframework all rights reserved.
 * http://www.gemframework.com
 * 版权所有，侵权必究！
 */
package com.gemframework.common.config.quartz;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@Component
@ConfigurationProperties("gem.quartz")
public class GemQuartzProperties {

	//任务类包路径
	private String jobClassPackagePath;

	//定时任务初始化方式：
	// 默认1:初始化全部停止状态；
	// 2:初始化全部运行状态，并启动任务
	// 3:保持原状态，并启动任务
	private Integer jobInit;

}
