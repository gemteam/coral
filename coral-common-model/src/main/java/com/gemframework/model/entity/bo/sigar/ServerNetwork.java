/**
 * 严肃声明：
 * 开源版本请务必保留此注释头信息，若删除捷码开源〔GEMOS〕官方保留所有法律责任追究！
 * 本软件受国家版权局知识产权以及国家计算机软件著作权保护（登记号：2018SR503328）
 * 不得恶意分享产品源代码、二次转售等，违者必究。
 * Copyright (c) 2020 gemframework all rights reserved.
 * http://www.gemframework.com
 * 版权所有，侵权必究！
 */
package com.gemframework.model.entity.bo.sigar;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.gemframework.model.enums.OssStorageType;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;


@Data
@Builder
public class ServerNetwork implements Serializable {

	private static final long serialVersionUID = 1L;

	//网络设备名:    lo0
	private String ethernetName;
	//IP地址:    127.0.0.1
	private String ipAddress;
	//子网掩码:    255.0.0.0
	private String netmask;
	private String broadcast;
	private String mac;
	private String type;
	private String description;
	//lo0接收的总包裹数:0
	private Long rxPackets;
	//lo0发送的总包裹数:0
	private Long txPackets;
	//lo0接收到的总字节数:0
	private Long rxBytes;
	//lo0发送的总字节数:0
	private Long txBytes;
	//lo0接收到的错误包数:0
	private Long rxErrors;
	//lo0发送数据包时的错误数:0
	private Long txErrors;
	//lo0接收时丢弃的包数:0
	private Long rxDropped;
	//lo0发送时丢弃的包数:0
	private Long txDropped;

}
