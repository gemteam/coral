/**
 * 开源版本请务必保留此注释头信息，若删除捷码开源〔GEMOS〕官方保留所有法律责任追究！
 * 本软件受国家版权局知识产权以及国家计算机软件著作权保护（登记号：2018SR503328）
 * 不得恶意分享产品源代码、二次转售等，违者必究。
 * Copyright (c) 2020 gemframework all rights reserved.
 * http://www.gemframework.com
 * 版权所有，侵权必究！
 */
package com.gemframework.model.response.sigar;

import lombok.Builder;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.io.Serializable;

@Slf4j
@Data
@Builder
public class HostJvmResponse implements Serializable {

    private static final long serialVersionUID = 1L;

    //Jvm名称	OpenJDK 64-Bit Server VM
    private String jvmName;
    //Java版本	1.8.0_111
    private String javaVersion;
    //启动时间	2020-01-13 18:17:05
    private String jvmStartTime;
    //运行时长	63天1小时45分钟
    private String jvmRunTime;
    //安装路径	/usr/lib/jvm/java-8-openjdk-amd64/jre
    private String javaHome;
    //项目路径	/
    private String appHome;

}
