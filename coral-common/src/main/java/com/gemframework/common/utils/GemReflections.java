package com.gemframework.common.utils;
/**
 * 开源版本请务必保留此注释头信息，若删除捷码开源〔GEMOS〕官方保留所有法律责任追究！
 * 本软件受国家版权局知识产权以及国家计算机软件著作权保护（登记号：2018SR503328）
 * 不得恶意分享产品源代码、二次转售等，违者必究。
 * Copyright (c) 2020 gemframework all rights reserved.
 * http://www.gemframework.com
 * 版权所有，侵权必究！
 */
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cglib.reflect.FastClass;
import org.springframework.cglib.reflect.FastMethod;

import java.lang.reflect.*;
import java.util.StringTokenizer;

/**
 * 反射工具类.
 * 
 * 提供调用getter/setter方法, 访问私有变量, 调用私有方法,
 * 获取泛型类型Class, 被AOP过的真实类等工具函数.
 * 
 */
@Slf4j
public class GemReflections {
	private static final String SETTER_PREFIX = "set";

	private static final String GETTER_PREFIX = "get";

	private static final String CGLIB_CLASS_SEPARATOR = "$$";

	private static Logger logger = LoggerFactory.getLogger(GemReflections.class);

	/**
	 * 调用Getter方法.
	 */
	public static Object invokeGetter(Object obj, String propertyName) {
		String getterMethodName = GETTER_PREFIX + StringUtils.capitalize(propertyName);
		return invokeMethod(obj, getterMethodName, new Class[] {}, new Object[] {});
	}

	/**
	 * 调用Setter方法, 仅匹配方法名。
	 */
	public static void invokeSetter(Object obj, String propertyName, Object value) {
		String setterMethodName = SETTER_PREFIX + StringUtils.capitalize(propertyName);
		invokeMethodByName(obj, setterMethodName, new Object[] { value });
	}

	/**
	 * 直接读取对象属性值, 无视private/protected修饰符, 不经过getter函数.
	 */
	public static Object getFieldValue(final Object obj, final String fieldName) {
		Field field = getAccessibleField(obj, fieldName);

		if (field == null) {
			throw new IllegalArgumentException("Could not find field [" + fieldName + "] on target [" + obj + "]");
		}

		Object result = null;
		try {
			result = field.get(obj);
		} catch (IllegalAccessException e) {
			logger.error("不可能抛出的异常{}", e.getMessage());
		}
		return result;
	}

	/**
	 * 直接设置对象属性值, 无视private/protected修饰符, 不经过setter函数.
	 */
	public static void setFieldValue(final Object obj, final String fieldName, final Object value) {
		Field field = getAccessibleField(obj, fieldName);

		if (field == null) {
			throw new IllegalArgumentException("Could not find field [" + fieldName + "] on target [" + obj + "]");
		}

		try {
			field.set(obj, value);
		} catch (IllegalAccessException e) {
			logger.error("不可能抛出的异常:{}", e.getMessage());
		}
	}

	/**
	 * 直接调用对象方法, 无视private/protected修饰符.
	 * 用于一次性调用的情况，否则应使用getAccessibleMethod()函数获得Method后反复调用.
	 * 同时匹配方法名+参数类型，
	 */
	public static Object invokeMethod(final Object obj, final String methodName, final Class<?>[] parameterTypes,
			final Object[] args) {
		Method method = getAccessibleMethod(obj, methodName, parameterTypes);
		if (method == null) {
			throw new IllegalArgumentException("Could not find method [" + methodName + "] on target [" + obj + "]");
		}

		try {
			return method.invoke(obj, args);
		} catch (Exception e) {
			throw convertReflectionExceptionToUnchecked(e);
		}
	}

	/**
	 * 直接调用对象方法, 无视private/protected修饰符，
	 * 用于一次性调用的情况，否则应使用getAccessibleMethodByName()函数获得Method后反复调用.
	 * 只匹配函数名，如果有多个同名函数调用第一个。
	 */
	public static Object invokeMethodByName(final Object obj, final String methodName, final Object[] args) {
		Method method = getAccessibleMethodByName(obj, methodName);
		if (method == null) {
			throw new IllegalArgumentException("Could not find method [" + methodName + "] on target [" + obj + "]");
		}

		try {
			return method.invoke(obj, args);
		} catch (Exception e) {
			throw convertReflectionExceptionToUnchecked(e);
		}
	}

	/**
	 * 循环向上转型, 获取对象的DeclaredField, 并强制设置为可访问.
	 * 
	 * 如向上转型到Object仍无法找到, 返回null.
	 */
	public static Field getAccessibleField(final Object obj, final String fieldName) {
		Validate.notNull(obj, "object can't be null");
		Validate.notBlank(fieldName, "fieldName can't be blank");
		for (Class<?> superClass = obj.getClass(); superClass != Object.class; superClass = superClass.getSuperclass()) {
			try {
				Field field = superClass.getDeclaredField(fieldName);
				makeAccessible(field);
				return field;
			} catch (NoSuchFieldException e) {// NOSONAR
				// Field不在当前类定义,继续向上转型
				log.error("e="+e.getMessage());
			}
		}
		return null;
	}

	/**
	 * 循环向上转型, 获取对象的DeclaredMethod,并强制设置为可访问.
	 * 如向上转型到Object仍无法找到, 返回null.
	 * 匹配函数名+参数类型。
	 * 
	 * 用于方法需要被多次调用的情况. 先使用本函数先取得Method,然后调用Method.invoke(Object obj, Object... args)
	 */
	public static Method getAccessibleMethod(final Object obj, final String methodName,
			final Class<?>... parameterTypes) {
		Validate.notNull(obj, "object can't be null");
		Validate.notBlank(methodName, "methodName can't be blank");

		for (Class<?> searchType = obj.getClass(); searchType != Object.class; searchType = searchType.getSuperclass()) {
			try {
				Method method = searchType.getDeclaredMethod(methodName, parameterTypes);
				makeAccessible(method);
				return method;
			} catch (NoSuchMethodException e) {
				// Method不在当前类定义,继续向上转型
			}
		}
		return null;
	}

	/**
	 * 循环向上转型, 获取对象的DeclaredMethod,并强制设置为可访问.
	 * 如向上转型到Object仍无法找到, 返回null.
	 * 只匹配函数名。
	 * 
	 * 用于方法需要被多次调用的情况. 先使用本函数先取得Method,然后调用Method.invoke(Object obj, Object... args)
	 */
	public static Method getAccessibleMethodByName(final Object obj, final String methodName) {
		Validate.notNull(obj, "object can't be null");
		Validate.notBlank(methodName, "methodName can't be blank");

		for (Class<?> searchType = obj.getClass(); searchType != Object.class; searchType = searchType.getSuperclass()) {
			Method[] methods = searchType.getDeclaredMethods();
			for (Method method : methods) {
				if (method.getName().equals(methodName)) {
					makeAccessible(method);
					return method;
				}
			}
		}
		return null;
	}

	/**
	 * 改变private/protected的方法为public，尽量不调用实际改动的语句，避免JDK的SecurityManager抱怨。
	 */
	public static void makeAccessible(Method method) {
		if ((!Modifier.isPublic(method.getModifiers()) || !Modifier.isPublic(method.getDeclaringClass().getModifiers()))
				&& !method.isAccessible()) {
			method.setAccessible(true);
		}
	}

	/**
	 * 改变private/protected的成员变量为public，尽量不调用实际改动的语句，避免JDK的SecurityManager抱怨。
	 */
	public static void makeAccessible(Field field) {
		if ((!Modifier.isPublic(field.getModifiers()) || !Modifier.isPublic(field.getDeclaringClass().getModifiers()) || Modifier
				.isFinal(field.getModifiers())) && !field.isAccessible()) {
			field.setAccessible(true);
		}
	}

	/**
	 * 通过反射, 获得Class定义中声明的泛型参数的类型, 注意泛型必须定义在父类处
	 * 如无法找到, 返回Object.class.
	 * eg.
	 * public UserDao extends HibernateDao<User>
	 * 
	 * @param clazz The class to introspect
	 * @return the first generic declaration, or Object.class if cannot be determined
	 */
	public static <T> Class<?> getClassGenricType(final Class<?> clazz) {
		return getClassGenricType(clazz, 0);
	}

	/**
	 * 通过反射, 获得Class定义中声明的父类的泛型参数的类型.
	 * 如无法找到, 返回Object.class.
	 * 
	 * 如public UserDao extends HibernateDao<User,Long>
	 * 
	 * @param clazz clazz The class to introspect
	 * @param index the Index of the generic ddeclaration,start from 0.
	 * @return the index generic declaration, or Object.class if cannot be determined
	 */
	public static Class<?> getClassGenricType(final Class<?> clazz, final int index) {

		Type genType = clazz.getGenericSuperclass();

		if (!(genType instanceof ParameterizedType)) {
			logger.warn(clazz.getSimpleName() + "'s superclass not ParameterizedType");
			return Object.class;
		}

		Type[] params = ((ParameterizedType) genType).getActualTypeArguments();

		if ((index >= params.length) || (index < 0)) {
			logger.warn("Index: " + index + ", Size of " + clazz.getSimpleName() + "'s Parameterized Type: "
					+ params.length);
			return Object.class;
		}
		if (!(params[index] instanceof Class)) {
			logger.warn(clazz.getSimpleName() + " not set the actual class on superclass generic parameter");
			return Object.class;
		}

		return (Class<?>) params[index];
	}

	public static Class<?> getUserClass(Object instance) {
		Validate.notNull(instance, "Instance must not be null");
		Class<?> clazz = instance.getClass();
		if ((clazz != null) && clazz.getName().contains(CGLIB_CLASS_SEPARATOR)) {
			Class<?> superClass = clazz.getSuperclass();
			if ((superClass != null) && !Object.class.equals(superClass)) {
				return superClass;
			}
		}
		return clazz;

	}

	/**
	 * 将反射时的checked exception转换为unchecked exception.
	 */
	public static RuntimeException convertReflectionExceptionToUnchecked(Exception e) {
		if ((e instanceof IllegalAccessException) || (e instanceof IllegalArgumentException)
				|| (e instanceof NoSuchMethodException)) {
			return new IllegalArgumentException(e);
		} else if (e instanceof InvocationTargetException) {
			return new RuntimeException(((InvocationTargetException) e).getTargetException());
		} else if (e instanceof RuntimeException) {
			return (RuntimeException) e;
		}
		return new RuntimeException("Unexpected Checked Exception.", e);
	}


	/**
	 * 调用对象方法
	 * 通过反射机制，根据对象的方法名、参数个数获取方法对象。然后将字符串参数按照参数类型进行转型。
	 * 然后调用对象方法。当前仅仅支持包装类型和int、long两种基本类型。该方法不支持重载函数，即同
	 * 一个函数名称，对应相同数量的不同类型的参数。该方法会自动选择第一个方法进行调用。
	 *
	 * @param obj        被调用对象
	 * @param clazz      被调用对象类型
	 * @param methodName 被调用方法名称
	 * @param parameters 传递参数数组
	 * @throws InvocationTargetException 方法调用失败，抛出该异常。该异常实际是FastMethod.invoke()抛出异常，而这一层不做处理。
	 * @throws NoSuchMethodException     如果该类型中，没有指定的方法名并且对应指定参数数量的方法；指定方法名和参数的方法被重载过。
	 *                                   则抛出该异常。
	 */
	public static Object invokeMethod(Object obj, Class clazz, String methodName, String[] parameters)
			throws InvocationTargetException, NoSuchMethodException {
		Method[] methods = clazz.getMethods();

		Method targetMethod = null;
		// 便利方法，获取一个方法参数数量与传递的参数数量一样的Method
		for (Method method : methods) {
			// 如果参数数量对应相同，则开始转换类型，并且调用方法
			if (method.getName().equals(methodName)
					&& method.getParameterTypes().length == parameters.length) {
				if (null == targetMethod) {
					targetMethod = method;
				} else {
					throw new NoSuchMethodException(
							clazz + "." + methodName + "() with "
									+ parameters.length
									+ " parameters  must be a unique method.");
				}
			}
		}

		// 判断目标方法是否存在，如果不存在，则抛出异常
		if (null == targetMethod) {
			throw new NoSuchMethodException(
					clazz + "." + methodName + "() with " + parameters.length
							+ " parameters.");
		}

		// 获取对象方法的参数类型数组
		Class[] parameterTypes = targetMethod.getParameterTypes();
		Object[] parameterObjects = new Object[parameterTypes.length];
		for (int i = 0; i < parameterTypes.length; i++) {
			if (null != parameterTypes[i].getComponentType()) {
				StringTokenizer st = new StringTokenizer(parameters[i], ",");
				String[] p = new String[st.countTokens()];
				int index = 0;
				while (st.hasMoreTokens()) {
					p[index++] = st.nextToken();
				}
				if (parameterTypes[i].getComponentType()
						.getName()
						.equals("int")) {
					parameterObjects[i] = warpParameter_int(p);
				} else if (parameterTypes[i].getComponentType()
						.getName()
						.equals("long")) {
					parameterObjects[i] = warpParameter_long(p);
				} else {
					parameterObjects[i] =
							warpParameterObject(parameterTypes[i].getComponentType(),
									p);
				}
			} else {
				parameterObjects[i] =
						warpParameterObject(parameterTypes[i], parameters[i]);
			}

		}

		return invokeFastMethod(obj, targetMethod, parameterObjects);
	}

	/**
	 * 包装参数
	 * @param clazz
	 * @param s
	 * @return
	 */
	private static Object warpParameterObject(Class clazz, String s) {
		final String clazzName = clazz.getName();

		// 首先判断基础数据类型
		if (clazzName.equals("int")) {
			return Integer.parseInt(s);
		} else if (clazzName.equals("long")) {
			return Long.parseLong(s);
		} else if (clazz.equals(Integer.class)) {
			return Integer.valueOf(s);
		} else if (clazz.equals(Long.class)) {
			return Long.valueOf(s);
		} else {
			return s;
		}
	}

	/**
	 * 包装参数
	 * @param clazz
	 * @param s
	 * @return
	 */
	private static <T> T[] warpParameterObject(Class<T> clazz, String[] s) {
		T[] ret = (T[]) java.lang.reflect.Array.newInstance(clazz, s.length);
		for (int i = 0; i < s.length; i++) {
			ret[i] = (T) warpParameterObject(clazz, s[i]);
		}
		return ret;
	}

	private static int[] warpParameter_int(String[] s) {
		int[] ret = new int[s.length];
		for (int i = 0; i < s.length; i++) {
			ret[i] = Integer.parseInt(s[i]);
		}
		return ret;
	}

	private static long[] warpParameter_long(String[] s) {
		long[] ret = new long[s.length];
		for (int i = 0; i < s.length; i++) {
			ret[i] = Integer.parseInt(s[i]);
		}
		return ret;
	}

	/**
	 * 基于cglib的FastMethod来实现反射调用
	 * @param obj        对象
	 * @param method     方法对象
	 * @param parameters 参数
	 * @throws InvocationTargetException
	 */
	private static Object invokeFastMethod(Object obj, Method method,Object[] parameters)
			throws InvocationTargetException {
		FastClass fc = FastClass.create(obj.getClass());
		FastMethod fm = fc.getMethod(method);
		return fm.invoke(obj, parameters);
	}

}
