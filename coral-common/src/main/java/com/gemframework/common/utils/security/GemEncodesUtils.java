//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//
package com.gemframework.common.utils.security;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.lang3.StringEscapeUtils;
import reactor.core.Exceptions;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;

/**
 * @Title: GemEncodesUtils
 * @Package: com.gemframework.common.utils
 * @Date: 2020-06-26 23:43:40
 * @Version: v1.0
 * @Description: 编码工具
 * @Author: nine QQ 769990999
 * @Copyright: Copyright (c) 2020 wanyong
 * @Company: www.gemframework.com
 */
@Slf4j
public class GemEncodesUtils {
    private static final String DEFAULT_URL_ENCODING = "UTF-8";
    private static final String baseDigits = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    private static final int BASE = baseDigits.length();
    private static final char[] digitsChar = baseDigits.toCharArray();
    private static final int FAST_SIZE = 123;
    private static final int[] digitsIndex = new int[FAST_SIZE];

    public GemEncodesUtils() {
    }

    public static String encodeHex(byte[] input) {
        return Hex.encodeHexString(input);
    }

    public static byte[] decodeHex(String input) {
        try {
            return Hex.decodeHex(input.toCharArray());
        } catch (DecoderException var2) {
            log.error(var2.getMessage());
            return null;
        }
    }

    public static String encodeBase64(byte[] input) {
        return Base64.encodeBase64String(input);
    }

    public static String encodeUrlSafeBase64(byte[] input) {
        return Base64.encodeBase64URLSafeString(input);
    }

    public static byte[] decodeBase64(String input) {
        return Base64.decodeBase64(input);
    }

    public static long decodeBase62(String input) {
        long result = 0L;
        long multiplier = 1L;

        for(int pos = input.length() - 1; pos >= 0; --pos) {
            int index = getIndex(input, pos);
            result += (long)index * multiplier;
            multiplier *= (long)BASE;
        }

        return result;
    }

    public static String encodeBase62(long number) {
        if (number < 0L) {
            throw new IllegalArgumentException("Number(Base62) must be positive: " + number);
        } else if (number == 0L) {
            return "0";
        } else {
            StringBuilder buf;
            for(buf = new StringBuilder(); number != 0L; number /= (long)BASE) {
                buf.append(digitsChar[(int)(number % (long)BASE)]);
            }

            return buf.reverse().toString();
        }
    }

    private static int getIndex(String input, int pos) {
        char chars = input.charAt(pos);
        if (chars > 'z') {
            throw new IllegalArgumentException("Unknow character for Base62: " + input);
        } else {
            int index = digitsIndex[chars];
            if (index == -1) {
                throw new IllegalArgumentException("Unknow character for Base62: " + input);
            } else {
                return index;
            }
        }
    }

    public static String escapeHtml(String html) {
        return StringEscapeUtils.escapeHtml4(html);
    }

    public static String unescapeHtml(String htmlEscaped) {
        return StringEscapeUtils.unescapeHtml4(htmlEscaped);
    }

    public static String escapeXml(String xml) {
        return StringEscapeUtils.escapeXml(xml);
    }

    public static String unescapeXml(String xmlEscaped) {
        return StringEscapeUtils.unescapeXml(xmlEscaped);
    }

    public static String urlEncode(String part) {
        try {
            return URLEncoder.encode(part, DEFAULT_URL_ENCODING);
        } catch (UnsupportedEncodingException var2) {
            log.error(var2.getMessage());
            return null;
        }
    }

    public static String urlDecode(String part) {
        try {
            return URLDecoder.decode(part, DEFAULT_URL_ENCODING);
        } catch (UnsupportedEncodingException var2) {
            log.error(var2.getMessage());
            return null;
        }
    }

    static {
        int i;
        for(i = 0; i < 122; ++i) {
            digitsIndex[i] = -1;
        }
        for(i = 0; i < BASE; digitsIndex[digitsChar[i]] = i++) {
        }

    }
}
