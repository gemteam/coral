/**
 * 开源版本请务必保留此注释头信息，若删除捷码开源〔GEMOS〕官方保留所有法律责任追究！
 * 本软件受国家版权局知识产权以及国家计算机软件著作权保护（登记号：2018SR503328）
 * 不得恶意分享产品源代码、二次转售等，违者必究。
 * Copyright (c) 2020 gemframework all rights reserved.
 * http://www.gemframework.com
 * 版权所有，侵权必究！
 */
package com.gemframework.common.config.datasource;

import lombok.extern.slf4j.Slf4j;

/**
 * @Title: DBContextHolder
 * @Package: com.gemframework.common.config.druid
 * @Date: 2020-06-29 21:03:33
 * @Version: v1.0
 * @Description: 切换数据源
 * @Author: nine QQ 769990999
 * @Copyright: Copyright (c) 2020 wanyong
 * @Company: www.gemframework.com
 */
@Slf4j
public class DBContextHolder {
    // 对当前线程的操作-线程安全的
    private static final ThreadLocal<String> contextHolder = new ThreadLocal<String>();
 
    // 调用此方法，切换数据源
    public static void setDataSource(String dataSource) {
        contextHolder.set(dataSource);
        log.info("已切换到数据源:{}",dataSource);
    }
 
    // 获取数据源
    public static String getDataSource() {
        return contextHolder.get();
    }
 
    // 删除数据源
    public static void clearDataSource() {
        contextHolder.remove();
        log.debug("已切换到主数据源");
    }
 
}