/**
 * 开源版本请务必保留此注释头信息，若删除捷码开源〔GEMOS〕官方保留所有法律责任追究！
 * 本软件受国家版权局知识产权以及国家计算机软件著作权保护（登记号：2018SR503328）
 * 不得恶意分享产品源代码、二次转售等，违者必究。
 * Copyright (c) 2020 gemframework all rights reserved.
 * http://www.gemframework.com
 * 版权所有，侵权必究！
 */
package com.gemframework.service.impl;
import com.gemframework.common.exception.GemException;
import com.gemframework.common.utils.GemRedisUtils;
import com.gemframework.common.utils.GemUUIDUtils;
import com.gemframework.model.common.ApiToken;
import com.gemframework.service.TokenService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import static com.gemframework.common.utils.GemCacheUtils.*;
import static com.gemframework.model.enums.ExceptionCode.*;

@Slf4j
@Service
public class TokenServiceImpl implements TokenService {

    @Autowired
    GemRedisUtils gemRedisUtils;

    private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    // AccessToken有效期限 单位秒
    public static final int ACCESSTOKEN_EXPIRE = 7200;

    // RefreshToken有效期限 单位小时
    private static final int REFRESHTOKEN_EXPIRE = 7;


    /**
     * @Title: refreshToken
     * @Description: 刷新token
     * @param memberId
     * @param refreshToken
     */
    public ApiToken refreshToken(Long memberId, String refreshToken){
        String cacheRefreshToken = (String) gemRedisUtils.get(getRefreshTokenKeyByMemberId(memberId));
        if(cacheRefreshToken==null){
            throw new GemException(REFRESH_TOKEN_INVALID);
        }
        if(!refreshToken.equals(cacheRefreshToken)){
            throw new GemException(REFRESH_TOKEN_ERROR);
        }
        ApiToken token = createToken(memberId,GemUUIDUtils.uuid2(),GemUUIDUtils.uuid2());
        return token;
    }

    /**
     * @Title: creatToken
     * @Description: 创建token
     * @param memberId
     */
    public ApiToken createToken(Long memberId,String accessToken,String refreshToken){
        return createToken(memberId, accessToken, refreshToken, ACCESSTOKEN_EXPIRE);
    }

    /**
     * 创建Token
     * @param memberId
     * @param accessToken
     * @param refreshToken
     * @param accesstokenExpire 单位秒
     * @return
     */
    @Override
    public ApiToken createToken(Long memberId, String accessToken, String refreshToken, Integer accesstokenExpire) {
        ApiToken token = new ApiToken();
        token.setMemberId(memberId);
        token.setAccessToken(accessToken);
        token.setRefreshToken(refreshToken);
        Date now = new Date();
        try {
            now = sdf.parse(sdf.format(now));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar oneHour = Calendar.getInstance();
        oneHour.setTime(now);
        oneHour.add(Calendar.SECOND, accesstokenExpire);
        token.setAccessTokenExpireTime(oneHour.getTime());
        Calendar oneWeek = Calendar.getInstance();
        oneWeek.setTime(now);
        oneWeek.add(Calendar.DAY_OF_YEAR, REFRESHTOKEN_EXPIRE);
        token.setRefreshTokenExpireTime(oneWeek.getTime());
        token.setUpdateTime(now);
        gemRedisUtils.setEx(getAccessTokenKeyByMemberId(memberId),token,accesstokenExpire, TimeUnit.SECONDS);
        //设置refreshToken
        gemRedisUtils.setEx(getRefreshTokenKeyByMemberId(memberId),token.getRefreshToken(),REFRESHTOKEN_EXPIRE, TimeUnit.DAYS);
        //设置会员ID redis key=accessToken:xxxx
        gemRedisUtils.setEx(getTokenKey(token.getAccessToken()),memberId,ACCESSTOKEN_EXPIRE, TimeUnit.SECONDS);
        return token;
    }

    @Override
    public ApiToken getAccessToken(Long memberId) {
        ApiToken token = this.getAccessToken(memberId,GemUUIDUtils.uuid2(),GemUUIDUtils.uuid2());
        return token;
    }

    @Override
    public ApiToken getAccessToken(Long memberId,String accessToken,String refreshToken) {
        return getAccessToken(memberId,accessToken,refreshToken,ACCESSTOKEN_EXPIRE);
    }

    @Override
    public ApiToken getAccessToken(Long memberId,String accessToken,String refreshToken,Integer accesstokenExpire) {
        //获取token
        ApiToken token = (ApiToken) gemRedisUtils.get(getAccessTokenKeyByMemberId(memberId));
        //token失效
        if(token==null){
            if(StringUtils.isBlank(accessToken)){
                accessToken = GemUUIDUtils.uuid2();
            }
            if(StringUtils.isBlank(refreshToken)){
                refreshToken = GemUUIDUtils.uuid2();
            }
            token = createToken(memberId,accessToken,refreshToken,accesstokenExpire);
        }
        return token;
    }
}